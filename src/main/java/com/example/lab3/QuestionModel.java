package com.example.lab3;

import com.example.lab3.mypackage.AnswersKeyAnswer;
import com.example.lab3.mypackage.FewCorrectAnswers;
import com.example.lab3.mypackage.OneCorrectAnswer;
import com.example.lab3.mypackage.Question;

public class QuestionModel extends Question {

    public QuestionModel(){}
    public QuestionModel(Question question){
        this.setId(question.getId());
        this.setComplexity(question.getComplexity());
        this.setMaxScore(question.getMaxScore());
        this.setQuestionText(question.getQuestionText());
        this.setTimeForAnswer(question.getTimeForAnswer());
        this.setTypeOfQuestion(question.getTypeOfQuestion());
        if(question.getTypeOfQuestion().getOneCorrectAnswer() != null){
            textTypeOfQuestion = "oneCorrectAnswer";
        } else if(question.getTypeOfQuestion().getFewCorrectAnswers() != null){
            textTypeOfQuestion = "fewCorrectAnswers";
        } else {
            textTypeOfQuestion = "answersKeyAnswer";
        }
    }

    private String textTypeOfQuestion;

    public String getTextTypeOfQuestion() {
        return textTypeOfQuestion;
    }

    public void setTextTypeOfQuestion(String textTypeOfQuestion) {
        this.textTypeOfQuestion = textTypeOfQuestion;
    }
}
