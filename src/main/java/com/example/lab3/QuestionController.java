package com.example.lab3;

import com.example.lab3.mypackage.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

@Controller
public class QuestionController {
    private Tests tests;

    @RequestMapping(value = "/tests", method = RequestMethod.GET)
    public ModelAndView tests(Model model) throws JAXBException {
        tests = readTest("fors.xml");
        ModelAndView test = new ModelAndView("tests", "test", tests.getTest());
        return test;
    }

    @RequestMapping(value = "/deleteQuestion", method = RequestMethod.GET)
    public ModelAndView deleteQuestion(@RequestParam(name="questionId") String questionId, Model model) throws JAXBException {
        tests.getTest().deleteQuestion(questionId);
        writeTest("fors.xml", tests);
        ModelAndView test = new ModelAndView("tests", "test", tests.getTest());
        return test;
    }

    @RequestMapping(value = "/editQuestion", method = RequestMethod.GET)
    public ModelAndView editQuestion(@RequestParam(name="questionId") String questionId, Model model) throws JAXBException {
        QuestionModel modelObject = new QuestionModel(tests.getTest().findQuestionById(questionId));
        ModelAndView question = new ModelAndView("question_" + modelObject.getTextTypeOfQuestion(), "question", modelObject);
        return question;
    }

    @RequestMapping(value = "/addQuestion", method = RequestMethod.GET)
    public ModelAndView editQuestion(Model model) throws JAXBException {
        ModelAndView question = new ModelAndView("addQuestion", "question", new QuestionModel());
        return question;
    }

    @RequestMapping(value = "/saveQuestion", method = RequestMethod.POST)
    public ModelAndView saveQuestion(Model model, QuestionModel questionModel) throws JAXBException {
        Question newQuestion = tests.getTest().findQuestionById(questionModel.getId());
        if(newQuestion == null){
            newQuestion = new Question();
            newQuestion.setTypeOfQuestion(new TypeOfQuestion());
            if("oneCorrectAnswer".equals(questionModel.getTextTypeOfQuestion())){
                OneCorrectAnswer newAnswer = new OneCorrectAnswer();
                newAnswer.setAnswers(new Answers());
                newQuestion.getTypeOfQuestion().setOneCorrectAnswer(newAnswer);
            } else if("fewCorrectAnswers".equals(questionModel.getTextTypeOfQuestion())){
                newQuestion.getTypeOfQuestion().setFewCorrectAnswers(new FewCorrectAnswers());
            } else {
                newQuestion.getTypeOfQuestion().setAnswersKeyAnswer(new AnswersKeyAnswer());
            }
            newQuestion.setId(questionModel.getId());
            newQuestion.setComplexity(questionModel.getComplexity());
            newQuestion.setMaxScore(questionModel.getMaxScore());
            newQuestion.setQuestionText(questionModel.getQuestionText());
            newQuestion.setTimeForAnswer(questionModel.getTimeForAnswer());
            tests.getTest().getQuestion().add(newQuestion);
            writeTest("fors.xml", tests);
            tests = readTest("fors.xml");
            ModelAndView test = new ModelAndView("tests", "test", tests.getTest());
            return test;
        }
        newQuestion.setComplexity(questionModel.getComplexity());
        newQuestion.setMaxScore(questionModel.getMaxScore());
        newQuestion.setQuestionText(questionModel.getQuestionText());
        newQuestion.setTimeForAnswer(questionModel.getTimeForAnswer());
        writeTest("fors.xml", tests);
        ModelAndView test = new ModelAndView("redirect:/tests", "test", tests.getTest());
        return test;
    }

    @RequestMapping(value = "/saveQuestionOne", method = RequestMethod.POST)
    public ModelAndView saveQuestionOne(Model model, QuestionModel questionModel) throws JAXBException {
        Question newQuestion = tests.getTest().findQuestionById(questionModel.getId());
        newQuestion.setComplexity(questionModel.getComplexity());
        newQuestion.setMaxScore(questionModel.getMaxScore());
        newQuestion.setQuestionText(questionModel.getQuestionText());
        newQuestion.setTimeForAnswer(questionModel.getTimeForAnswer());
        newQuestion.getTypeOfQuestion().getOneCorrectAnswer().getAnswers().getCorrectAnswer().setId(questionModel.getTypeOfQuestion().getOneCorrectAnswer().getAnswers().getCorrectAnswer().getId());
        newQuestion.getTypeOfQuestion().getOneCorrectAnswer().getAnswers().getCorrectAnswer().setTextOfCorrectAnswer(questionModel.getTypeOfQuestion().getOneCorrectAnswer().getAnswers().getCorrectAnswer().getTextOfCorrectAnswer());
        newQuestion.getTypeOfQuestion().getOneCorrectAnswer().getAnswers().getCorrectAnswer().setWeightAnswer(questionModel.getTypeOfQuestion().getOneCorrectAnswer().getAnswers().getCorrectAnswer().getWeightAnswer());
        writeTest("fors.xml", tests);
        return new ModelAndView("redirect:/tests");
    }

    @RequestMapping(value = "/saveQuestionFew", method = RequestMethod.POST)
    public ModelAndView saveQuestionFew(Model model, QuestionModel questionModel) throws JAXBException {
        Question newQuestion = tests.getTest().findQuestionById(questionModel.getId());
        newQuestion.setComplexity(questionModel.getComplexity());
        newQuestion.setMaxScore(questionModel.getMaxScore());
        newQuestion.setQuestionText(questionModel.getQuestionText());
        newQuestion.setTimeForAnswer(questionModel.getTimeForAnswer());
        writeTest("fors.xml", tests);
        return new ModelAndView("redirect:/tests");
    }

    @RequestMapping(value = "/saveQuestionKeyWord", method = RequestMethod.POST)
    public ModelAndView saveQuestionKeyWord(Model model, QuestionModel questionModel) throws JAXBException {
        Question newQuestion = tests.getTest().findQuestionById(questionModel.getId());
        newQuestion.setComplexity(questionModel.getComplexity());
        newQuestion.setMaxScore(questionModel.getMaxScore());
        newQuestion.setQuestionText(questionModel.getQuestionText());
        newQuestion.setTimeForAnswer(questionModel.getTimeForAnswer());
        newQuestion.getTypeOfQuestion().getAnswersKeyAnswer().setId(questionModel.getTypeOfQuestion().getAnswersKeyAnswer().getId());
        newQuestion.getTypeOfQuestion().getAnswersKeyAnswer().setAnswerTextKeyWord(questionModel.getTypeOfQuestion().getAnswersKeyAnswer().getAnswerTextKeyWord());
        newQuestion.getTypeOfQuestion().getAnswersKeyAnswer().setWeightAnswer(questionModel.getTypeOfQuestion().getAnswersKeyAnswer().getWeightAnswer());
        writeTest("fors.xml", tests);
        return new ModelAndView("redirect:/tests");
    }

    @RequestMapping(value = "/deleteAnswer", method = RequestMethod.GET)
    public ModelAndView deleteAnswer(@RequestParam(name="questionId") String questionId, @RequestParam(name="answerId") String answerId, Model model) throws JAXBException {
        Question question = tests.getTest().findQuestionById(questionId);
        if (question != null){
            question.deleteAnswer(answerId);
        }
        writeTest("fors.xml", tests);
        return new ModelAndView("redirect:/editQuestion?questionId=" + questionId);
    }

    @RequestMapping(value = "/editAnswer", method = RequestMethod.GET)
    public ModelAndView editAnswer(@RequestParam(name="questionId") String questionId, @RequestParam(name="answerId") String answerId, Model model) throws JAXBException {
        TypeOfQuestion typeOfQuestion = tests.getTest().findQuestionById(questionId).getTypeOfQuestion();
        if (typeOfQuestion.getFewCorrectAnswers() != null ) {
            return new ModelAndView("addAnswer", "answer", new AnswerModel(questionId, typeOfQuestion.getFewCorrectAnswers().findAnswerById(answerId)));
        } else {
            return new ModelAndView("addAnswer", "answer", new AnswerModel(questionId, typeOfQuestion.getOneCorrectAnswer().getAnswers().findAnswerById(answerId)));
        }
    }

    @RequestMapping(value = "/addAnswer", method = RequestMethod.GET)
    public ModelAndView editAnswer(@RequestParam(name="questionId") String questionId, Model model) throws JAXBException {
        return new ModelAndView("addAnswer", "answer", new AnswerModel(questionId));
    }

    @RequestMapping(value = "/saveEditedAnswer", method = RequestMethod.POST)
    public ModelAndView saveEditedAnswer(Model model, AnswerModel answerModel) throws JAXBException {
        Question question = tests.getTest().findQuestionById(answerModel.getQuestionId());
        if (question != null){
            question.editAnswer(answerModel);
        }
        writeTest("fors.xml", tests);
        return new ModelAndView("redirect:/editQuestion?questionId=" + answerModel.getQuestionId());
    }

    @RequestMapping(value = "/editTestInfo", method = RequestMethod.POST)
    public ModelAndView editTestInfo(Model model, Test test) throws JAXBException {
        tests.getTest().setId(test.getId());
        tests.getTest().setNumberOfQuestions(test.getNumberOfQuestions());
        tests.getTest().setNumberOfQuestionsInTest(test.getNumberOfQuestionsInTest());
        tests.getTest().setScoreForTest(test.getScoreForTest());
        tests.getTest().setTheme(test.getTheme());
        tests.getTest().setTimeForTest(test.getTimeForTest());
        writeTest("fors.xml", tests);
        ModelAndView testModelView = new ModelAndView("tests", "test", tests.getTest());
        return testModelView;
    }

    private Tests readTest(String fileName) throws JAXBException {
        File file = new File(fileName);
        JAXBContext jaxbContext1 = JAXBContext.newInstance(Tests.class);
        Unmarshaller unmarshaller = jaxbContext1.createUnmarshaller();
        return  (Tests) unmarshaller.unmarshal(file);
    }

    private void writeTest(String fileName, Tests tests) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Tests.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(tests, new File(fileName));
        jaxbMarshaller.marshal(tests, System.out);
    }

}
