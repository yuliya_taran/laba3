
package com.example.lab3.mypackage;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}answer-text-key-word"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *       &lt;attribute name="weight-answer" type="{http://www.w3.org/2001/XMLSchema}integer" default="4" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "answerTextKeyWord"
})
@XmlRootElement(name = "ответ-ключевое-слово")
public class AnswersKeyAnswer {

    @XmlElement(name = "текст-ответа-ключевое-слово", required = true)
    protected String answerTextKeyWord;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String id;
    @XmlAttribute(name = "вес-ответа")
    protected BigInteger weightAnswer;

    /**
     * Gets the value of the answerTextKeyWord property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnswerTextKeyWord() {
        return answerTextKeyWord;
    }

    /**
     * Sets the value of the answerTextKeyWord property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnswerTextKeyWord(String value) {
        this.answerTextKeyWord = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the weightAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getWeightAnswer() {
        if (weightAnswer == null) {
            return new BigInteger("4");
        } else {
            return weightAnswer;
        }
    }

    /**
     * Sets the value of the weightAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setWeightAnswer(BigInteger value) {
        this.weightAnswer = value;
    }

}
