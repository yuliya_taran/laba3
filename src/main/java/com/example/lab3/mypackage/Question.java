
package com.example.lab3.mypackage;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}complexity"/>
 *         &lt;element ref="{}time-for-answer"/>
 *         &lt;element ref="{}question-text"/>
 *         &lt;element ref="{}max-score"/>
 *         &lt;element ref="{}type-of-question"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "complexity",
    "timeForAnswer",
    "questionText",
    "maxScore",
    "typeOfQuestion"
})
@XmlRootElement(name = "вопрос-теста")
public class Question {

    @XmlElement(name = "сложность-вопроса", required = true)
    protected int complexity;
    @XmlElement(name = "время-отводимое-на-ответ", required = true)
    protected BigInteger timeForAnswer;
    @XmlElement(name = "текст-вопроса", required = true)
    protected String questionText;
    @XmlElement(name = "максимальный-балл")
    protected int maxScore;
    @XmlElement(name = "вид-вопроса", required = true)
    protected TypeOfQuestion typeOfQuestion = new TypeOfQuestion();
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String id;

    /**
     * Gets the value of the complexity property.
     * 
     */
    public int getComplexity() {
        return complexity;
    }

    /**
     * Sets the value of the complexity property.
     * 
     */
    public void setComplexity(int value) {
        this.complexity = value;
    }

    /**
     * Gets the value of the timeForAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTimeForAnswer() {
        return timeForAnswer;
    }

    /**
     * Sets the value of the timeForAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTimeForAnswer(BigInteger value) {
        this.timeForAnswer = value;
    }

    /**
     * Gets the value of the questionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionText() {
        return questionText;
    }

    /**
     * Sets the value of the questionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionText(String value) {
        this.questionText = value;
    }

    /**
     * Gets the value of the maxScore property.
     * 
     */
    public int getMaxScore() {
        return maxScore;
    }

    /**
     * Sets the value of the maxScore property.
     * 
     */
    public void setMaxScore(int value) {
        this.maxScore = value;
    }

    /**
     * Gets the value of the typeOfQuestion property.
     * 
     * @return
     *     possible object is
     *     {@link TypeOfQuestion }
     *     
     */
    public TypeOfQuestion getTypeOfQuestion() {
        return typeOfQuestion;
    }

    /**
     * Sets the value of the typeOfQuestion property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeOfQuestion }
     *     
     */
    public void setTypeOfQuestion(TypeOfQuestion value) {
        this.typeOfQuestion = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    public boolean deleteAnswer(String answerId) {
        if(typeOfQuestion.getOneCorrectAnswer() != null
                && typeOfQuestion.getOneCorrectAnswer().getAnswers() != null) {
            return typeOfQuestion.getOneCorrectAnswer().getAnswers().removeById(answerId);
        }
        if(typeOfQuestion.getFewCorrectAnswers() != null) {
            return typeOfQuestion.getFewCorrectAnswers().removeById(answerId);
        }
        return false;
    }

    public boolean editAnswer(AnswerModel answerModel) {
        if(typeOfQuestion.getOneCorrectAnswer() != null
                && typeOfQuestion.getOneCorrectAnswer().getAnswers() != null) {
            typeOfQuestion.getOneCorrectAnswer().getAnswers().edit(answerModel);
            return true;
        }
        if(typeOfQuestion.getFewCorrectAnswers() != null) {
            typeOfQuestion.getFewCorrectAnswers().edit(answerModel);
            return true;
        }
        return false;
    }
}
