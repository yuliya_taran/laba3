
package com.example.lab3.mypackage;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}theme"/>
 *         &lt;element ref="{}number-of-questions"/>
 *         &lt;element ref="{}number-of-questions-in-test"/>
 *         &lt;element ref="{}time-for-test"/>
 *         &lt;element ref="{}score-for-test"/>
 *         &lt;element ref="{}question" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = {
//    "theme",
//    "numberOfQuestions",
//    "numberOfQuestionsInTest",
//    "timeForTest",
//    "scoreForTest"
//})
@XmlRootElement(name = "тест")
public class Test {

    @XmlElement(name = "тема", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String theme;
    @XmlElement(name = "количество-задаваемых-вопросов", required = true)
    protected BigInteger numberOfQuestions;
    @XmlElement(name = "количество-вопросов-в-тесте", required = true)
    protected BigInteger numberOfQuestionsInTest;
    @XmlElement(name = "общее-время-тестирования", required = true)
    protected BigInteger timeForTest;
    @XmlElement(name = "общее-количество-баллов-за-тест", required = true)
    protected BigInteger scoreForTest;
    @XmlElement(name = "вопрос-теста", required = true)
    protected List<Question> question;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String id;

    /**
     * Gets the value of the theme property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheme() {
        return theme;
    }

    /**
     * Sets the value of the theme property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheme(String value) {
        this.theme = value;
    }

    /**
     * Gets the value of the numberOfQuestions property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfQuestions() {
        return numberOfQuestions;
    }

    /**
     * Sets the value of the numberOfQuestions property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfQuestions(BigInteger value) {
        this.numberOfQuestions = value;
    }

    /**
     * Gets the value of the numberOfQuestionsInTest property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfQuestionsInTest() {
        return numberOfQuestionsInTest;
    }

    /**
     * Sets the value of the numberOfQuestionsInTest property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfQuestionsInTest(BigInteger value) {
        this.numberOfQuestionsInTest = value;
    }

    /**
     * Gets the value of the timeForTest property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTimeForTest() {
        return timeForTest;
    }

    /**
     * Sets the value of the timeForTest property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTimeForTest(BigInteger value) {
        this.timeForTest = value;
    }

    /**
     * Gets the value of the scoreForTest property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getScoreForTest() {
        return scoreForTest;
    }

    /**
     * Sets the value of the scoreForTest property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setScoreForTest(BigInteger value) {
        this.scoreForTest = value;
    }

    /**
     * Gets the value of the question property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the question property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuestion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Question }
     * 
     * 
     */
    public List<Question> getQuestion() {
        if (question == null) {
            question = new ArrayList<Question>();
        }
        return this.question;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    public void deleteQuestion(String questionId) {
        question.remove(findQuestionById(questionId));
    }

    public Question findQuestionById(String questionId) {

        return question.stream().filter(q-> questionId.equals(q.getId())).findFirst().orElse(null);
    }
}
