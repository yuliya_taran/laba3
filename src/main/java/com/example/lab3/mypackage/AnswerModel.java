package com.example.lab3.mypackage;

import java.math.BigInteger;

public class AnswerModel {
    private String text;
    private String id;
    private String questionId;
    private BigInteger weightAnswer;

    public AnswerModel(){}
    public AnswerModel(String questionId){
        this.questionId = questionId;
    }
    public AnswerModel(String questionId, Answer answer){
        this.questionId = questionId;
        this.id = answer.getId();
        this.text = answer.getAnswerText();
        this.weightAnswer = answer.getWeightAnswer();
    }
    public AnswerModel(String questionId, IncorrectAnswer answer){
        this.questionId = questionId;
        this.id = answer.getId();
        this.text = answer.getTextOfIncorrectAnswer();
        this.weightAnswer = answer.getWeightAnswer();
    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public BigInteger getWeightAnswer() {
        return weightAnswer;
    }

    public void setWeightAnswer(BigInteger weightAnswer) {
        this.weightAnswer = weightAnswer;
    }
}
