
package com.example.lab3.mypackage;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}correct-answer"/>
 *         &lt;element ref="{}incorrect-answer" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "correctAnswer",
    "incorrectAnswer"
})
@XmlRootElement(name = "ответы")
public class Answers {

    @XmlElement(name = "правельный-ответ", required = true)
    protected CorrectAnswer correctAnswer;
    @XmlElement(name = "неправельный-ответ", required = true)
    protected List<IncorrectAnswer> incorrectAnswer;

    /**
     * Gets the value of the correctAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectAnswer }
     *     
     */
    public CorrectAnswer getCorrectAnswer() {
        return correctAnswer;
    }

    /**
     * Sets the value of the correctAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectAnswer }
     *     
     */
    public void setCorrectAnswer(CorrectAnswer value) {
        this.correctAnswer = value;
    }

    /**
     * Gets the value of the incorrectAnswer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the incorrectAnswer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncorrectAnswer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IncorrectAnswer }
     * 
     * 
     */
    public List<IncorrectAnswer> getIncorrectAnswer() {
        if (incorrectAnswer == null) {
            incorrectAnswer = new ArrayList<IncorrectAnswer>();
        }
        return this.incorrectAnswer;
    }

    public boolean removeById(String answerId) {
        if (incorrectAnswer == null){
            return false;
        }
        IncorrectAnswer answer = findAnswerById(answerId);
        return answer == null ? false : incorrectAnswer.remove(answer);
    }

    public IncorrectAnswer findAnswerById(String answerId) {
        if (incorrectAnswer == null){
            return null;
        }
        return incorrectAnswer.stream().filter(a-> answerId.equals(a.getId())).findFirst().orElse(null);
    }

    public void edit(AnswerModel answerModel) {
        incorrectAnswer = incorrectAnswer != null ? incorrectAnswer : new ArrayList<IncorrectAnswer>();
        IncorrectAnswer editedAnswer = findAnswerById(answerModel.getId());
        if(editedAnswer == null) {
            editedAnswer = new IncorrectAnswer();
            editedAnswer.setId(answerModel.getId());
            incorrectAnswer.add(editedAnswer);
        }
        editedAnswer.setTextOfIncorrectAnswer(answerModel.getText());
        editedAnswer.setWeightAnswer(answerModel.getWeightAnswer());
    }
}
