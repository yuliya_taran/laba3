
package com.example.lab3.mypackage;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}answer" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "answer"
})
@XmlRootElement(name = "несколько-правильных-ответов")
public class FewCorrectAnswers {

    @XmlElement(name = "ответ", required = true)
    protected List<Answer> answer;

    /**
     * Gets the value of the answer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the answer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnswer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Answer }
     * 
     * 
     */
    public List<Answer> getAnswer() {
        if (answer == null) {
            answer = new ArrayList<Answer>();
        }
        return this.answer;
    }

    public boolean removeById(String answerId) {
        if (answer == null){
            return false;
        }
        Answer answerDel = findAnswerById(answerId);
        return answer == null ? false : answer.remove(answerDel);
    }

    public Answer findAnswerById(String answerId) {
        if (answer == null){
            return null;
        }
        return answer.stream().filter(a-> answerId.equals(a.getId())).findFirst().orElse(null);
    }

    public void edit(AnswerModel answerModel) {
        answer = answer != null ? answer : new ArrayList<Answer>();
        Answer editedAnswer = findAnswerById(answerModel.getId());
        if(editedAnswer == null) {
            editedAnswer = new Answer();
            editedAnswer.setId(answerModel.getId());
            answer.add(editedAnswer);
        }
        editedAnswer.setAnswerText(answerModel.getText());
        editedAnswer.setWeightAnswer(answerModel.getWeightAnswer());
    }
}
