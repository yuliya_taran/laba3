
package com.example.lab3.mypackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}test"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = {
//    "test"
//})
@XmlRootElement(name = "тесты")
public class Tests {

    @XmlElement(name="тест", required = true)
    protected Test test;

    /**
     * Gets the value of the test property.
     *
     * @return
     *     possible object is
     *     {@link Test }
     *
     */
    public Test getTest() {
        return test;
    }

    /**
     * Sets the value of the test property.
     *
     * @param value
     *     allowed object is
     *     {@link Test }
     *
     */
    public void setTest(Test value) {
        this.test = value;
    }

}
