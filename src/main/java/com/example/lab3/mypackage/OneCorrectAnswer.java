
package com.example.lab3.mypackage;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}answers"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "answers"
})
@XmlRootElement(name = "один-правильный-ответ")
public class OneCorrectAnswer {

    @XmlElement(name = "ответы", required = true)
    protected Answers answers;

    /**
     * Gets the value of the answers property.
     * 
     * @return
     *     possible object is
     *     {@link Answers }
     *     
     */
    public Answers getAnswers() {
        return answers;
    }

    /**
     * Sets the value of the answers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Answers }
     *     
     */
    public void setAnswers(Answers value) {
        this.answers = value;
    }

}
