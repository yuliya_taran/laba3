
package com.example.lab3.mypackage;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.namespace.QName;
import java.math.BigInteger;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mypackage package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ScoreForTest_QNAME = new QName("", "score-for-test");
    private final static QName _Complexity_QNAME = new QName("", "complexity");
    private final static QName _QuestionText_QNAME = new QName("", "question-text");
    private final static QName _NumberOfQuestions_QNAME = new QName("", "number-of-questions");
    private final static QName _MaxScore_QNAME = new QName("", "max-score");
    private final static QName _TimeForAnswer_QNAME = new QName("", "time-for-answer");
    private final static QName _TextOfIncorrectAnswer_QNAME = new QName("", "text-of-incorrect-answer");
    private final static QName _NumberOfQuestionsInTest_QNAME = new QName("", "number-of-questions-in-test");
    private final static QName _AnswerText_QNAME = new QName("", "answer-text");
    private final static QName _AnswerTextKeyWord_QNAME = new QName("", "answer-text-key-word");
    private final static QName _TextOfCorrectAnswer_QNAME = new QName("", "text-of-correct-answer");
    private final static QName _TimeForTest_QNAME = new QName("", "time-for-test");
    private final static QName _Theme_QNAME = new QName("", "theme");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mypackage
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CorrectAnswer }
     * 
     */
    public CorrectAnswer createCorrectAnswer() {
        return new CorrectAnswer();
    }

    /**
     * Create an instance of {@link AnswersKeyAnswer }
     * 
     */
    public AnswersKeyAnswer createAnswersKeyAnswer() {
        return new AnswersKeyAnswer();
    }

    /**
     * Create an instance of {@link Test }
     * 
     */
    public Test createTest() {
        return new Test();
    }

    /**
     * Create an instance of {@link Question }
     * 
     */
    public Question createQuestion() {
        return new Question();
    }

    /**
     * Create an instance of {@link TypeOfQuestion }
     * 
     */
    public TypeOfQuestion createTypeOfQuestion() {
        return new TypeOfQuestion();
    }

    /**
     * Create an instance of {@link OneCorrectAnswer }
     * 
     */
    public OneCorrectAnswer createOneCorrectAnswer() {
        return new OneCorrectAnswer();
    }

    /**
     * Create an instance of {@link Answers }
     * 
     */
    public Answers createAnswers() {
        return new Answers();
    }

    /**
     * Create an instance of {@link IncorrectAnswer }
     * 
     */
    public IncorrectAnswer createIncorrectAnswer() {
        return new IncorrectAnswer();
    }

    /**
     * Create an instance of {@link FewCorrectAnswers }
     * 
     */
    public FewCorrectAnswers createFewCorrectAnswers() {
        return new FewCorrectAnswers();
    }

    /**
     * Create an instance of {@link Answer }
     * 
     */
    public Answer createAnswer() {
        return new Answer();
    }

    /**
     * Create an instance of {@link Tests }
     * 
     */
    public Tests createTests() {
        return new Tests();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "score-for-test")
    public JAXBElement<BigInteger> createScoreForTest(BigInteger value) {
        return new JAXBElement<BigInteger>(_ScoreForTest_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "complexity")
    public JAXBElement<Integer> createComplexity(Integer value) {
        return new JAXBElement<Integer>(_Complexity_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "question-text")
    public JAXBElement<String> createQuestionText(String value) {
        return new JAXBElement<String>(_QuestionText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "number-of-questions")
    public JAXBElement<BigInteger> createNumberOfQuestions(BigInteger value) {
        return new JAXBElement<BigInteger>(_NumberOfQuestions_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "max-score")
    public JAXBElement<Integer> createMaxScore(Integer value) {
        return new JAXBElement<Integer>(_MaxScore_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "time-for-answer")
    public JAXBElement<BigInteger> createTimeForAnswer(BigInteger value) {
        return new JAXBElement<BigInteger>(_TimeForAnswer_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "text-of-incorrect-answer")
    public JAXBElement<String> createTextOfIncorrectAnswer(String value) {
        return new JAXBElement<String>(_TextOfIncorrectAnswer_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "number-of-questions-in-test")
    public JAXBElement<BigInteger> createNumberOfQuestionsInTest(BigInteger value) {
        return new JAXBElement<BigInteger>(_NumberOfQuestionsInTest_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "answer-text")
    public JAXBElement<String> createAnswerText(String value) {
        return new JAXBElement<String>(_AnswerText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "answer-text-key-word")
    public JAXBElement<String> createAnswerTextKeyWord(String value) {
        return new JAXBElement<String>(_AnswerTextKeyWord_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "text-of-correct-answer")
    public JAXBElement<String> createTextOfCorrectAnswer(String value) {
        return new JAXBElement<String>(_TextOfCorrectAnswer_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "time-for-test")
    public JAXBElement<BigInteger> createTimeForTest(BigInteger value) {
        return new JAXBElement<BigInteger>(_TimeForTest_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "theme")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createTheme(String value) {
        return new JAXBElement<String>(_Theme_QNAME, String.class, null, value);
    }

}
