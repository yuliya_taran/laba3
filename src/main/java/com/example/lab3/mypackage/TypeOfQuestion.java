
package com.example.lab3.mypackage;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{}one-correct-answer"/>
 *         &lt;element ref="{}few-correct-answers"/>
 *         &lt;element ref="{}answers-key-answer"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "oneCorrectAnswer",
    "fewCorrectAnswers",
    "answersKeyAnswer"
})
@XmlRootElement(name = "вид-вопроса")
public class TypeOfQuestion {

    @XmlElement(name = "один-правильный-ответ")
    protected OneCorrectAnswer oneCorrectAnswer;
    @XmlElement(name = "несколько-правильных-ответов")
    protected FewCorrectAnswers fewCorrectAnswers;
    @XmlElement(name = "ответ-ключевое-слово")
    protected AnswersKeyAnswer answersKeyAnswer;

    /**
     * Gets the value of the oneCorrectAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link OneCorrectAnswer }
     *     
     */
    public OneCorrectAnswer getOneCorrectAnswer() {
        return oneCorrectAnswer;
    }

    /**
     * Sets the value of the oneCorrectAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link OneCorrectAnswer }
     *     
     */
    public void setOneCorrectAnswer(OneCorrectAnswer value) {
        this.oneCorrectAnswer = value;
    }

    /**
     * Gets the value of the fewCorrectAnswers property.
     * 
     * @return
     *     possible object is
     *     {@link FewCorrectAnswers }
     *     
     */
    public FewCorrectAnswers getFewCorrectAnswers() {
        return fewCorrectAnswers;
    }

    /**
     * Sets the value of the fewCorrectAnswers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FewCorrectAnswers }
     *     
     */
    public void setFewCorrectAnswers(FewCorrectAnswers value) {
        this.fewCorrectAnswers = value;
    }

    /**
     * Gets the value of the answersKeyAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link AnswersKeyAnswer }
     *     
     */
    public AnswersKeyAnswer getAnswersKeyAnswer() {
        return answersKeyAnswer;
    }

    /**
     * Sets the value of the answersKeyAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswersKeyAnswer }
     *     
     */
    public void setAnswersKeyAnswer(AnswersKeyAnswer value) {
        this.answersKeyAnswer = value;
    }

}
